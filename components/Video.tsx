import { StyleProp, StyleSheet, Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import React from 'react';
import { MediaStream, RTCView } from 'react-native-webrtc';
import Button from './Button';
import Ionicons from 'react-native-vector-icons/Ionicons';

interface Ivideo {
    hangup: () => void;
    localStream?: MediaStream | null;
    remoteStream?: MediaStream | null;
    switchCamera: () => void;
    toggleMute: () => void,
    isMute: boolean,
}
const ButtonContainer = ({ hangup, iconName, backgroundColor, style }: { hangup: () => void, iconName: string, backgroundColor: string, style?: StyleProp<ViewStyle> }) => {
    return <View style={styles.bContainer}>
        <Button
            iconName={iconName}
            backgroundColor={backgroundColor}
            onPress={hangup}
            style={[style, { marginHorizontal: 30 }]}

        />

    </View>
}

const Video = ({ hangup, localStream, remoteStream, toggleMute, switchCamera, isMute }: Ivideo) => {
    if (localStream && !remoteStream) {
        return <View style={styles.container}>
            <RTCView streamURL={localStream.toURL()} objectFit='cover' style={styles.video} />
            <ButtonContainer hangup={hangup} iconName='phone' backgroundColor='red' />

        </View>
    }
    if (localStream && remoteStream) {
        return <View style={styles.container}>

            <RTCView streamURL={remoteStream.toURL()} objectFit='cover' style={styles.video} />
            <RTCView streamURL={localStream.toURL()} objectFit='cover' style={styles.videoLocal} />
            <ButtonContainer hangup={hangup} iconName='phone' backgroundColor='red' />
            <TouchableOpacity style={{ position: 'absolute', right: 20, bottom: 40 }}>
                <Ionicons size={40} name={'camera-reverse-outline'} color={'white'} onPress={switchCamera} />
            </TouchableOpacity>
            <TouchableOpacity style={{ position: 'absolute', right: 20, bottom: 140 }}>
                <Ionicons size={40} name={isMute ? 'mic-off' : 'mic'} color={'white'} onPress={toggleMute} />
            </TouchableOpacity>
        </View>
    }
    return null;






};

export default Video;

const styles = StyleSheet.create({
    bContainer: {
        flexDirection: 'row',
        bottom: 40,
        zIndex: 10
    },
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    video: {
        position: 'absolute',
        height: '100%',
        width: '100%'
    },
    videoLocal: {
        position: 'absolute',
        width: 100,
        height: 150,
        top: 0,
        left: 20,
        elevation: 10
    }
});
