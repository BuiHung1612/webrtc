import { StyleProp, StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View, ViewProps, ViewStyle } from 'react-native';
import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


interface IButton {
    onPress: () => void;
    iconName: string,
    backgroundColor: string,
    style?: StyleProp<ViewStyle>,
    disable?: boolean,
}
const Button = ({ onPress, iconName, backgroundColor, style, disable }: IButton) => {
    return (
        <View>
            <TouchableOpacity disabled={disable} onPress={onPress} style={[style, { backgroundColor: backgroundColor, }, styles.btn,]}>
                <FontAwesome name={iconName} size={20} color={'white'} />
            </TouchableOpacity>

        </View>
    );
};

export default Button;

const styles = StyleSheet.create({
    btn: {
        width: 60,
        height: 60,
        padding: 10,
        elevation: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },
    btnWithoutColor: {
        width: 60,
        height: 60,



    }
});
