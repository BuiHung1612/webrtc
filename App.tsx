import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Vibration,
    Alert,
    LogBox,
    Platform
} from 'react-native';
import React, { useEffect, useRef, useState } from 'react';
import GettingCall from './components/GettingCall';
import Video from './components/Video';
import {
    EventOnAddStream,
    MediaStream,
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
} from 'react-native-webrtc';
import Button from './components/Button';
import Utils from './Utils';
import firestore, {
    FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';
import uuid from 'uuid';
import RNCallKeep from 'react-native-callkeep';
import BackgroundTimer from 'react-native-background-timer';
import DeviceInfo from 'react-native-device-info';

BackgroundTimer.start();
const configuration = {
    iceServers: [
        { urls: 'stun:stun.l.google.com:19302' },
        {
            url: 'turn:numb.viagenie.ca',
            credential: 'muazkh',
            username: 'webrtc@live.com',
        },
    ],
};

const getNewUuid = () => uuid.v4().toLowerCase();

const format = (uuid: string) => uuid.split('-')[0];

const getRandomNumber = () => String(Math.floor(Math.random() * 100000));

const isIOS = Platform.OS === 'ios';

RNCallKeep.setup({
    ios: {
        appName: 'CallKeepDemo',
    },
    android: {
        alertTitle: 'Permissions required',
        alertDescription: 'This application needs to access your phone accounts',
        cancelButton: 'Cancel',
        okButton: 'ok',
    },
});

const App = () => {

    const [localStream, setLocalStream] = useState<MediaStream | null>();
    const [remoteStream, setRemoteStream] = useState<MediaStream | null>();
    const [gettingCall, setGettingCall] = useState(false);
    const [roomId, setRoomId] = useState('');
    const [isMuted, setIsMuted] = useState(false);

    const pc = useRef<RTCPeerConnection>();
    const connecting = useRef(false);
    const [logText, setLog] = useState('');
    const [heldCalls, setHeldCalls] = useState({}); // callKeep uuid: held
    const [mutedCalls, setMutedCalls] = useState({}); // callKeep uuid: muted
    const [calls, setCalls] = useState({}); // callKeep uuid: number

    const log = (text: string) => {
        console.info(text);
        setLog(logText + "\n" + text);
    };



    const hangup = async () => {
        setGettingCall(false);
        connecting.current = false;
        streamCleanUp();
        firestoreCleanUp();
        if (pc.current) {
            pc.current.close();
        }

    };

    const streamCleanUp = async () => {
        if (localStream) {
            localStream.getTracks().forEach(t => t.stop());
            localStream.release();
        }
        setLocalStream(null);
        setRemoteStream(null);
    };
    const firestoreCleanUp = async () => {
        const cRef = firestore().collection('meet').doc(roomId);
        if (cRef) {
            const calleeCandidate = await cRef.collection('callee').get();
            calleeCandidate.forEach(async candidate => {
                await candidate.ref.delete();
            });
            const callerCandidate = await cRef.collection('caller').get();
            callerCandidate.forEach(async candidate => {
                await candidate.ref.delete();
            });

            cRef.delete();
        }
    };
    useEffect(() => {
        const cRef = firestore().collection('meet').doc(roomId);
        const callUUID = getNewUuid();
        const subscribe = cRef.onSnapshot(snap => {
            const data = snap.data();
            if (pc.current && !pc.current.remoteDescription && data && data.answer) {
                pc.current.setRemoteDescription(new RTCSessionDescription(data.answer));
            }

            if (data && data.offer && !connecting.current) {
                displayIncomingCallNow()
            }
            if (!data) {
                hangup()

            }

        });

        const subscribeDelete = cRef.collection('callee').onSnapshot(snap => {
            snap.docChanges().forEach(change => {
                if (change.type == 'removed') {
                    setTimeout(() => {
                        hangup();
                    }, 2000);

                }
            });
        });
        return () => {
            subscribe();
            subscribeDelete();
        };
    }, [roomId]);

    const setupWebrtc = async () => {
        pc.current = new RTCPeerConnection(configuration);
        //get the audio and video stream for call
        const stream = await Utils.getStream();
        if (stream) {
            setLocalStream(stream);
            pc.current.addStream(stream);
        }
        // get the remote stream once it in visible
        pc.current.onaddstream = (event: EventOnAddStream) => {
            setRemoteStream(event.stream);
        };
    };

    const switchCamera = () => {
        localStream?.getVideoTracks().forEach(track => track._switchCamera());
    };

    // Mutes the local's outgoing audio
    const toggleMute = () => {
        if (!remoteStream) {
            return;
        }
        localStream?.getAudioTracks().forEach(track => {
            track.enabled = !track.enabled;
            setIsMuted(!track.enabled);
        });
    };

    const create = async () => {
        console.log('calling');
        connecting.current = true;
        await setupWebrtc();
        // document for call
        const cRef = firestore().collection('meet').doc(roomId);
        // exchange the ICE candidates bettwen the caller and callee
        collectIceCandidates(cRef, 'caller', 'callee');

        if (pc.current) {
            // create the offer for the call
            // store the offer under the document
            const offer = await pc.current.createOffer()
            pc.current.setLocalDescription(offer);
            const cWithOffer = {
                offer: {
                    type: offer.type,
                    sdp: offer.sdp,
                },
            };
            cRef.set(cWithOffer);
        }
    };
    const join = async () => {
        console.log('joining');
        connecting.current = true;
        setGettingCall(false);
        const cRef = firestore().collection('meet').doc(roomId);
        const offer = (await cRef.get()).data()?.offer;

        if (offer) {
            await setupWebrtc();

            collectIceCandidates(cRef, 'callee', 'caller');
            if (pc.current) {
                pc.current.setRemoteDescription(new RTCSessionDescription(offer));
                const answer = await pc.current.createAnswer();
                pc.current.setLocalDescription(answer);
                const cWithAnswer = {
                    answer: {
                        type: answer.type,
                        sdp: answer.sdp,
                    },
                };
                cRef.update(cWithAnswer);
            }
        }
    };

    const collectIceCandidates = async (
        cRef: FirebaseFirestoreTypes.DocumentReference<FirebaseFirestoreTypes.DocumentData>,
        localName: string,
        remoteName: string,
    ) => {
        const candidateCollection = cRef.collection(localName);
        if (pc.current) {
            // on new ICE candidates add it to firestore
            pc.current.onicecandidate = event => {
                if (event.candidate) {
                    candidateCollection.add(event.candidate);
                }
            };
        }
        cRef.collection(remoteName).onSnapshot(snapshot => {
            snapshot.docChanges().forEach((change: any) => {
                if (change.type == 'added') {
                    const candidate = new RTCIceCandidate(change.doc.data());
                    pc.current?.addIceCandidate(candidate);
                }
            });
        });
    };



    const addCall = (callUUID: any, number: any) => {
        setHeldCalls({ ...heldCalls, [callUUID]: false });
        setCalls({ ...calls, [callUUID]: number });
    };

    const removeCall = (callUUID: string) => {
        setCalls({});
    };

    const displayIncomingCall = (number: string | undefined) => {
        const callUUID = getNewUuid();
        addCall(callUUID, number);
        RNCallKeep.displayIncomingCall(callUUID, 'Hùng Bùi', number, 'number', true);
    };

    const displayIncomingCallNow = () => {
        displayIncomingCall(getRandomNumber());
    };

    const answerCall = ({ callUUID }: any) => {

        RNCallKeep.backToForeground()
        join()
        RNCallKeep.endAllCalls();
        const number = calls[callUUID];
        log(`[answerCall] ${format(callUUID)}, number: ${number}`);
        RNCallKeep.startCall(callUUID, number, number);
        BackgroundTimer.setTimeout(() => {
            log(`[setCurrentCallActive] ${format(callUUID)}, number: ${number}`);
            RNCallKeep.setCurrentCallActive(callUUID);
        }, 1000);
    };
    const endCall = ({ callUUID }: any) => {
        RNCallKeep.endCall(callUUID)
        removeCall(callUUID);
    };

    useEffect(() => {
        RNCallKeep.addEventListener('answerCall', answerCall);
        RNCallKeep.addEventListener('endCall', endCall);

        return () => {
            RNCallKeep.removeEventListener('answerCall', answerCall);
            RNCallKeep.removeEventListener('endCall', endCall);
        }
    }, []);

    if (isIOS && DeviceInfo.isEmulator()) {
        return <Text style={styles.container}>CallKeep doesn't work on iOS emulator</Text>;
    }

    // if (gettingCall) {
    //     return <GettingCall hangup={hangup} join={join} />;
    // }
    if (localStream) {
        return (
            <Video
                hangup={hangup}
                localStream={localStream}
                remoteStream={remoteStream}
                switchCamera={switchCamera}
                toggleMute={toggleMute}
                isMute={isMuted}
            />
        );
    }

    return (
        <View style={styles.container}>
            <TextInput
                placeholder="Enter name"
                style={styles.txtInput}
                value={roomId}
                onChangeText={text => setRoomId(text)}
            />
            <Button iconName="video-camera" backgroundColor="gray" onPress={create} />
        </View>
    );
};

export default App;

const styles = StyleSheet.create({
    txtInput: {
        width: '90%',
        paddingVertical: 10,
        color: '#000',
        borderWidth: 1,
        marginBottom: 10,
    },
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        marginTop: 20,
        marginBottom: 20,
    },
    callButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        width: '100%',
    },
    logContainer: {
        flex: 3,
        width: '100%',
        backgroundColor: '#D9D9D9',
    },
    log: {
        fontSize: 10,
    }
});

