import { Image, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import Button from './Button';

interface IGettingCall {
    hangup: () => void;
    join: () => void;
}

const GettingCall = ({ hangup, join }: IGettingCall) => {
    return (
        <View style={styles.container}>
            <Image resizeMode='cover' source={require('../assets/images/animal2.jpeg')} style={styles.image} />
            <View style={{ flexDirection: 'row', bottom: 40 }}>
                <Button
                    iconName='phone'
                    backgroundColor='green'
                    onPress={join}
                    style={{ marginHorizontal: 30 }}
                />

                <Button
                    iconName='phone'
                    backgroundColor='red'
                    onPress={hangup}
                    style={{ marginHorizontal: 30 }}
                />
            </View>
        </View>
    );
};

export default GettingCall;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    image: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    }
});
